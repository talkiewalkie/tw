module.exports = ({ env }) => ({
  url: env("HEROKU_URL"),
  port: env("PORT"),
});
