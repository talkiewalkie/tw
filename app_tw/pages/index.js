import Head from "next/head";
import EventList from "../features/event/EventList";

const IndexPage = () => (
  <>
    <Head>
      <title>Talkie Walkie</title>
    </Head>
    <EventList />
  </>
);

export default IndexPage;
