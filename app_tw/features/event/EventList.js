import { gql, useQuery, NetworkStatus } from "@apollo/client";
import ErrorMessage from "../../components/shared/ErrorMessage";

export const ALL_EVENTS_QUERY = gql`
  query allEvents {
    #($first: Int!, $skip: Int!) {
    events {
      #(orderBy: { createdAt: desc }, first: $first, skip: $skip) {
      id
      title
      description
    }
  }
`;

export const allEventsQueryVars = {
  skip: 0,
  first: 10,
};

export default function EventList() {
  const { loading, error, data, fetchMore, networkStatus } = useQuery(
    ALL_EVENTS_QUERY,
    {
      variables: allEventsQueryVars,
      // Setting this value to true will make the component rerender when
      // the "networkStatus" changes, so we are able to know if it is fetching
      // more data
      notifyOnNetworkStatusChange: true,
    }
  );

  const loadingMoreEvents = networkStatus === NetworkStatus.fetchMore;

  const loadMoreEvents = () => {
    fetchMore({
      variables: {
        skip: allEvents.length,
      },
    });
  };

  if (error) return <ErrorMessage message="Error loading events." />;
  if (loading && !loadingMoreEvents) return <div>Loading</div>;

  const { events } = data;
  // const areMoreEvents = events.length < _eventsMeta.count;

  return (
    <section>
      <ul>
        {events.map((event, index) => (
          <li key={event.id}>
            <div>
              <span>{index + 1}. </span>
              <span>{event.title}</span>
            </div>
          </li>
        ))}
      </ul>
      <button onClick={() => loadMoreEvents()} disabled={loadingMoreEvents}>
        {loadingMoreEvents ? "Loading..." : "Show More"}
      </button>
      <style jsx>{`
        section {
          padding-bottom: 20px;
        }
        li {
          display: block;
          margin-bottom: 10px;
        }
        div {
          align-items: center;
          display: flex;
        }
        a {
          font-size: 14px;
          margin-right: 10px;
          text-decoration: none;
          padding-bottom: 0;
          border: 0;
        }
        span {
          font-size: 14px;
          margin-right: 5px;
        }
        ul {
          margin: 0;
          padding: 0;
        }
        button:before {
          align-self: center;
          border-style: solid;
          border-width: 6px 4px 0 4px;
          border-color: #ffffff transparent transparent transparent;
          content: "";
          height: 0;
          margin-right: 5px;
          width: 0;
        }
      `}</style>
    </section>
  );
}
